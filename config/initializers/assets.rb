Rails.application.config.assets.precompile += %w( admin/active_admin.css )

Rails.application.config.assets.precompile += %w( coffee/image.js admin/active_admin.js admin/sortable.js tiny_mce/tiny_mce.js )
