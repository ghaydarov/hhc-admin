Rails.application.routes.draw do
  

  resources :images do
    collection do
      get :result
    end
  end

  root 'home#index'
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)


  
  resources :exports do
    collection do
      get :event
    end
  end

  resources :slides
  resources :releases
  resources :events
  resources :tips
  resources :settings

  resources :whatsnews

  resources :layouts
end