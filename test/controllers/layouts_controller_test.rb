require 'test_helper'

class LayoutsControllerTest < ActionController::TestCase
  setup do
    @layout = layouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:layouts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create layout" do
    assert_difference('Layout.count') do
      post :create, layout: { body_bottom: @layout.body_bottom, body_top: @layout.body_top, head: @layout.head, layout_name: @layout.layout_name, og_description: @layout.og_description, og_image: @layout.og_image, og_title: @layout.og_title, sidebar: @layout.sidebar, title: @layout.title }
    end

    assert_redirected_to layout_path(assigns(:layout))
  end

  test "should show layout" do
    get :show, id: @layout
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @layout
    assert_response :success
  end

  test "should update layout" do
    patch :update, id: @layout, layout: { body_bottom: @layout.body_bottom, body_top: @layout.body_top, head: @layout.head, layout_name: @layout.layout_name, og_description: @layout.og_description, og_image: @layout.og_image, og_title: @layout.og_title, sidebar: @layout.sidebar, title: @layout.title }
    assert_redirected_to layout_path(assigns(:layout))
  end

  test "should destroy layout" do
    assert_difference('Layout.count', -1) do
      delete :destroy, id: @layout
    end

    assert_redirected_to layouts_path
  end
end
