require 'test_helper'

class WhatsnewsControllerTest < ActionController::TestCase
  setup do
    @whatsnews = whatsnews(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:whatsnews)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create whatsnews" do
    assert_difference('Whatsnew.count') do
      post :create, whatsnews: { content: @whatsnews.content, image: @whatsnews.image, link: @whatsnews.link, title: @whatsnews.title }
    end

    assert_redirected_to whatsnews_path(assigns(:whatsnews))
  end

  test "should show whatsnews" do
    get :show, id: @whatsnews
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @whatsnews
    assert_response :success
  end

  test "should update whatsnews" do
    patch :update, id: @whatsnews, whatsnews: { content: @whatsnews.content, image: @whatsnews.image, link: @whatsnews.link, title: @whatsnews.title }
    assert_redirected_to whatsnews_path(assigns(:whatsnews))
  end

  test "should destroy whatsnews" do
    assert_difference('Whatsnew.count', -1) do
      delete :destroy, id: @whatsnews
    end

    assert_redirected_to whatsnews_path
  end
end
