class AddFacilityNameToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :facility_name, :string
  end
end
