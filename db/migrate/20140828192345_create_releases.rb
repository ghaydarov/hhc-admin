class CreateReleases < ActiveRecord::Migration
  def change
    create_table :releases do |t|
      t.string :title
      t.text :short
      t.text :content
      t.string :image_url
      t.string :slug
      t.string :date

      t.timestamps
    end
  end
end
