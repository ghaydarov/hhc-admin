class CreateLayouts < ActiveRecord::Migration
  def change
    create_table :layouts do |t|
      t.string :layout_name
      t.text :head
      t.string :title
      t.string :og_title
      t.text :og_description
      t.string :og_image
      t.text :body_top
      t.text :body_bottom
      t.string :sidebar

      t.timestamps
    end
  end
end
