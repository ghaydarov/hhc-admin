class AddLayoutIdToWhatsnews < ActiveRecord::Migration
  def change
    add_column :whatsnews, :layout_id, :integer
  end
end
