class AddAdminUserIdToTips < ActiveRecord::Migration
  def change
    add_column :tips, :admin_user_id, :integer
  end
end
