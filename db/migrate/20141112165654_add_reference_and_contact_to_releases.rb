class AddReferenceAndContactToReleases < ActiveRecord::Migration
  def change
    add_column :releases, :reference, :text
    add_column :releases, :contact, :text
  end
end
