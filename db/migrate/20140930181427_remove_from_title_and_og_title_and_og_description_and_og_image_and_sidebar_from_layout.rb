class RemoveFromTitleAndOgTitleAndOgDescriptionAndOgImageAndSidebarFromLayout < ActiveRecord::Migration
  def change
    remove_column :layouts, :title, :string
    remove_column :layouts, :og_title, :string
    remove_column :layouts, :og_description, :text
    remove_column :layouts, :og_image, :string
    remove_column :layouts, :sidebar, :string
  end
end
