class AddShortToTips < ActiveRecord::Migration
  def change
    add_column :tips, :short, :text
  end
end
