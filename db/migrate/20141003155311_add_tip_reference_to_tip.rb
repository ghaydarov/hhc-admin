class AddTipReferenceToTip < ActiveRecord::Migration
  def change
    add_column :tips, :tip_reference, :text
  end
end
