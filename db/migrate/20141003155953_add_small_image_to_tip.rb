class AddSmallImageToTip < ActiveRecord::Migration
  def change
    add_column :tips, :small_image, :string
  end
end
