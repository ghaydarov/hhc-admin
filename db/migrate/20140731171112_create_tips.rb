class CreateTips < ActiveRecord::Migration
  def change
    create_table :tips do |t|
      t.string :title
      t.string :imge_url
      t.text :content

      t.timestamps
    end
  end
end
