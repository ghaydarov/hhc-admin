class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :hero_thumb

      t.timestamps
    end
  end
end
