class AddFacebookTitleToReleases < ActiveRecord::Migration
  def change
    add_column :releases, :facebook_title, :string
  end
end
