class CreateExports < ActiveRecord::Migration
  def change
    create_table :exports do |t|
      t.string :title
      t.string :link
      t.string :content
      t.string :template

      t.timestamps
    end
  end
end
