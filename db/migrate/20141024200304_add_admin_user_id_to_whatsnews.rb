class AddAdminUserIdToWhatsnews < ActiveRecord::Migration
  def change
    add_column :whatsnews, :admin_user_id, :integer
  end
end
