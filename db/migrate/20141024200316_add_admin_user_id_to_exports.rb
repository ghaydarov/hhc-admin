class AddAdminUserIdToExports < ActiveRecord::Migration
  def change
    add_column :exports, :admin_user_id, :integer
  end
end
