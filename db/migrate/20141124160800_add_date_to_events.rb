class AddDateToEvents < ActiveRecord::Migration
  def change
    add_column :events, :month_year, :date
  end
end
