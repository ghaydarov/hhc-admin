class AddLayoutIdToSlide < ActiveRecord::Migration
  def change
    add_column :slides, :layout_id, :integer
  end
end
