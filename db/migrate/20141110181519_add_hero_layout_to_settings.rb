class AddHeroLayoutToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :hero_layout, :string
  end
end
