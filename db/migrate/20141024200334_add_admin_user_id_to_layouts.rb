class AddAdminUserIdToLayouts < ActiveRecord::Migration
  def change
    add_column :layouts, :admin_user_id, :integer
  end
end
