class RemoveTipReferenceFromExport < ActiveRecord::Migration
  def change
    remove_column :exports, :tip_reference, :text
  end
end
