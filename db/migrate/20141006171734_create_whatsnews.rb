class CreateWhatsnews < ActiveRecord::Migration
  def change
    create_table :whatsnews do |t|
      t.string :title
      t.string :image
      t.text :content
      t.string :link

      t.timestamps
    end
  end
end
