class AddTipReferenceToExport < ActiveRecord::Migration
  def change
    add_column :exports, :tip_reference, :text
  end
end
