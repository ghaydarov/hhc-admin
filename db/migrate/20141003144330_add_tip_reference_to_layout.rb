class AddTipReferenceToLayout < ActiveRecord::Migration
  def change
    add_column :layouts, :tip_reference, :text
  end
end
