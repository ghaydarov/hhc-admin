class AddWhatsnewIdToExport < ActiveRecord::Migration
  def change
    add_column :exports, :whatsnew_id, :integer
  end
end
