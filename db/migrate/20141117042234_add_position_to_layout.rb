class AddPositionToLayout < ActiveRecord::Migration
  def change
    add_column :layouts, :position, :integer
  end
end
