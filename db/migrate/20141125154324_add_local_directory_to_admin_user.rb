class AddLocalDirectoryToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :local_directory, :string
  end
end
