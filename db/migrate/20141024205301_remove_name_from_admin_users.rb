class RemoveNameFromAdminUsers < ActiveRecord::Migration
  def change
    remove_column :admin_users, :name, :string
  end
end
