class AddLayoutIdToTip < ActiveRecord::Migration
  def change
    add_column :tips, :layout_id, :integer
  end
end
