class AddStatusToWhatsnew < ActiveRecord::Migration
  def change
    add_column :whatsnews, :status, :integer, default: 0
  end
end
