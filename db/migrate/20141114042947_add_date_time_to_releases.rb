class AddDateTimeToReleases < ActiveRecord::Migration
  def change
    add_column :releases, :date_time, :string
  end
end
