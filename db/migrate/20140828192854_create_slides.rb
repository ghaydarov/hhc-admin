class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :title
      t.text :description
      t.string :link
      t.string :image_url
      t.string :thumb_url
      t.string :slug
      t.string :date

      t.timestamps
    end
  end
end
