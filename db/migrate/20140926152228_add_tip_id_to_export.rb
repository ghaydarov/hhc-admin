class AddTipIdToExport < ActiveRecord::Migration
  def change
    add_column :exports, :tip_id, :integer
  end
end
