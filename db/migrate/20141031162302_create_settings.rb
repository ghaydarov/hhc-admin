class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :name
      t.string :healthy_tip_layout
      t.string :healthy_tip_archive_layout
      t.string :in_the_news_layout
      t.string :in_the_news_archive_layout

      t.timestamps
    end
  end
end
