class AddLayoutIdToExport < ActiveRecord::Migration
  def change
    add_column :exports, :layout_id, :integer
  end
end
