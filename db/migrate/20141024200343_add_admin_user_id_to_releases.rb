class AddAdminUserIdToReleases < ActiveRecord::Migration
  def change
    add_column :releases, :admin_user_id, :integer
  end
end
