class AddAdminUserIdToSlides < ActiveRecord::Migration
  def change
    add_column :slides, :admin_user_id, :integer
  end
end
