class AddPositionToReleases < ActiveRecord::Migration
  def change
    add_column :releases, :position, :integer
  end
end
