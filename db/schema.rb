# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141125211738) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role"
    t.string   "facility_name"
    t.string   "local_directory"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "events", force: true do |t|
    t.string   "title"
    t.text     "short"
    t.text     "content"
    t.string   "image_url"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_user_id"
    t.integer  "status"
    t.integer  "position"
    t.date     "month_year"
  end

  create_table "exports", force: true do |t|
    t.string   "title"
    t.string   "link"
    t.string   "content"
    t.string   "template"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "layout_id"
    t.integer  "tip_id"
    t.integer  "whatsnew_id"
    t.integer  "admin_user_id"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "images", force: true do |t|
    t.string   "hero_thumb"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "layouts", force: true do |t|
    t.string   "layout_name"
    t.text     "head"
    t.text     "body_top"
    t.text     "body_bottom"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "tip_reference"
    t.integer  "admin_user_id"
    t.integer  "position"
  end

  create_table "releases", force: true do |t|
    t.string   "title"
    t.text     "short"
    t.text     "content"
    t.string   "image_url"
    t.string   "slug"
    t.string   "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_user_id"
    t.text     "reference"
    t.text     "contact"
    t.string   "date_time"
    t.integer  "status"
    t.integer  "position"
    t.string   "facebook_title"
  end

  create_table "settings", force: true do |t|
    t.string   "name"
    t.string   "healthy_tip_layout"
    t.string   "healthy_tip_archive_layout"
    t.string   "in_the_news_layout"
    t.string   "in_the_news_archive_layout"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "hero_layout"
  end

  create_table "slides", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "link"
    t.string   "image_url"
    t.string   "thumb_url"
    t.string   "slug"
    t.string   "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_user_id"
    t.integer  "layout_id"
    t.integer  "position"
    t.integer  "status",        default: 0
  end

  create_table "templates", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tips", force: true do |t|
    t.string   "title"
    t.string   "imge_url"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.text     "short"
    t.text     "tip_reference"
    t.string   "small_image"
    t.integer  "layout_id"
    t.integer  "admin_user_id"
  end

  create_table "whatsnews", force: true do |t|
    t.string   "title"
    t.string   "image"
    t.text     "content"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_user_id"
    t.integer  "layout_id"
    t.integer  "position"
    t.integer  "status",        default: 0
  end

end
