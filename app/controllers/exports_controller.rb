class ExportsController < ApplicationController
  before_action :set_export, only: [:show, :edit, :update, :destroy]
  def index
    @exports = Export.all
  end

  def show
      @export = Export.find(params[:id])
      @tip = @export.tip
  end

  def new
    @export = Export.new
  end

  def edit
  end




  def update
    respond_to do |format|
      if @export.update(export_params)
        format.html { redirect_to @export, notice: 'Export was successfully updated.' }
        format.json { render :show, status: :ok, location: @export }
      else
        format.html { render :edit }
        format.json { render json: @export.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @export.destroy
    respond_to do |format|
      format.html { redirect_to exports_url, notice: 'Export was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_export
      @export = Export.find(params[:id])
    end

    def export_params
      params.require(:export).permit(:title, :content, :link, :template, :layout_id, :tip_id)
    end
end
