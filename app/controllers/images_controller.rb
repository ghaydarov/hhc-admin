class ImagesController < InheritedResources::Base



    def update
    	@image = Image.find(params[:id])
    	if @image.update(image_params)
    		redirect_to result_images_url(id: @image.id)
    	end	
    	
    end

    def result
    	@image = Image.find(params[:id])
    end



  private
    def image_params
      params.require(:image).permit(:hero_thumb, :crop_x, :crop_y, :crop_w, :crop_h)
    end
end

