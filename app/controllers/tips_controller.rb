class TipsController < InheritedResources::Base


		before_action :authenticate_admin_user!
		def index
			@tips = Tip.all
			@title = "this is me"
			render json: @tips
		end

		def show
			@tip = Tip.find(params[:id])
			render json: @tip
		end
end
