json.array!(@whatsnews) do |whatsnews|
  json.extract! whatsnews, :id, :title, :image, :content, :link
  json.url whatsnews_url(whatsnews, format: :json)
end
