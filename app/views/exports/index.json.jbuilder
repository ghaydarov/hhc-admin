json.array!(@exports) do |export|
  json.extract! export, :id, :title, :link, :content, :template
  json.url export_url(export, format: :json)
end
