json.array!(@events) do |event|
  json.extract! event, :id, :title, :short, :content, :image_url, :slug, :date
  json.url event_url(event, format: :json)
end
