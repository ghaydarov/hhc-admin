json.array!(@releases) do |release|
  json.extract! release, :id, :title, :short, :content, :image_url, :slug, :date
  json.url release_url(release, format: :json)
end
