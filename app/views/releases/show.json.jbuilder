json.extract! @release, :id, :title, :short, :content, :image_url, :slug, :date, :created_at, :updated_at
