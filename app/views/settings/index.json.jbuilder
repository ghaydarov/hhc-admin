json.array!(@settings) do |setting|
  json.extract! setting, :id, :name, :healthy_tip_layout, :healthy_tip_archive_layout, :in_the_news_layout, :in_the_news_archive_layout
  json.url setting_url(setting, format: :json)
end
