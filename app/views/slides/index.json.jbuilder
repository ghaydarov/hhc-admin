json.array!(@slides) do |slide|
  json.extract! slide, :id, :title, :description, :link, :image_url, :thumb_url, :slug, :date
  json.url slide_url(slide, format: :json)
end
