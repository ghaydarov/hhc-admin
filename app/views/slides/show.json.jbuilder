json.extract! @slide, :id, :title, :description, :link, :image_url, :thumb_url, :slug, :date, :created_at, :updated_at
