json.extract! @layout, :id, :layout_name, :head, :title, :og_title, :og_description, :og_image, :body_top, :body_bottom, :sidebar, :created_at, :updated_at
