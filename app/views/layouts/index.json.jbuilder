json.array!(@layouts) do |layout|
  json.extract! layout, :id, :layout_name, :head, :title, :og_title, :og_description, :og_image, :body_top, :body_bottom, :sidebar
  json.url layout_url(layout, format: :json)
end
