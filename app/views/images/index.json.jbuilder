json.array!(@images) do |image|
  json.extract! image, :id, :hero_thumb
  json.url image_url(image, format: :json)
end
