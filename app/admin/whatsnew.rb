ActiveAdmin.register Whatsnew do
  
  menu :label => "Whats News"
  menu :parent => "Home Page"

  permit_params :title, :content, :image, :link, :admin_user_id, :layout_id
  config.filters = false
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported
  sortable # creates the controller action which handles the sorting
  
  scope :active, default: true
  scope :archived


  member_action :archive, :method => :put do
  wn = Whatsnew.find(params[:id])
    if wn.nonarchive? || wn.status == nil
      wn.archive!
      redirect_to admin_whatsnews_index_url
    end
  end

  member_action :activate, :method => :put do
  wn = Whatsnew.find(params[:id])
    if wn.archive? || wn.status == nil
      wn.nonarchive!
      redirect_to admin_whatsnews_index_url
    end
  end

  index do
    sortable_handle_column # inserts a drag handle
    column :position
    column :title
    column "change status" do |wn|
      if wn.archive? 
        link_to "activate", activate_admin_whatsnews_url(wn.id), :method => :put
      else
        link_to "archive it", archive_admin_whatsnews_url(wn.id), :method => :put
      end
    end    

    actions
  end


  form do |f|
    f.inputs do
      f.input :title
      f.input :admin_user_id, :as => :hidden, :value => AdminUser.find(current_admin_user).id
      f.input :content, :input_html => { :class => 'noEditor' }
      f.input :link, :input_html => { placeholder: '../../html/folder/file.shtml'}
      f.input :image, label: 'Image URL', :input_html => { placeholder: '../../images/whatsnew-image.jpg'}
      # unless f.object.new_record?
      #   f.input :slug
      # end
    end
      f.actions
  end  

  # scope_to :current_admin_user
  
end