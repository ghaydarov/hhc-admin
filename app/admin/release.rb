ActiveAdmin.register Release do

  menu :label => "Press Releases"
  config.filters = false
  permit_params :title, :short, :content, :image_url, :slug, :date, :reference, :contact, :date_time, :status, :facebook_title
  scope_to :current_admin_user
  
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported
  sortable # creates the controller action which handles the sorting


  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  member_action :feature, :method => :put do
  old_featured_press = Release.find_by(status: 0)
  event = Release.find(params[:id])
    unless old_featured_press.nil?
      old_featured_press.regular!    
    end
      event.update(position: 1, status: 0)
      redirect_to admin_releases_url
  end

  member_action :regular, :method => :put do
  press = Release.find(params[:id])
    if press.featured?
      press.regular!
      redirect_to admin_releases_url
    end
  end

  index do
    sortable_handle_column # inserts a drag handle

    column :position
    column :title
    column :status
    # column :short
    # column :content do |release|
    #   truncate(release.content, omision: "...", length: 30)
    # end
    column :image_url  do |release|
      image_tag(release.image_url, width: 175)
    end    
    # column "Date", :date
    # column "URL", :slug

    column "change status" do |press|
      unless press.featured? 
        link_to "make featured", feature_admin_release_url(press.id), :method => :put
      end

    end

    # column "Release Date", :released_at
    # column :price, :sortable => :price do |product|
    #   div :class => "price" do
    #     number_to_currency product.price
    #   end
    # end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :short, :input_html => { :class => 'noEditor' }      
      f.input :date_time      
      f.input :content  
      f.input :status, as: :select, :include_blank => false, collection: Release.statuses.keys.to_a.reverse      
      f.input :reference
      f.input :contact
      f.input :facebook_title
      f.input :image_url
      unless f.object.new_record?
        f.input :slug
      end
    end
      f.actions
  end
end
