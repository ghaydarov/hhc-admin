ActiveAdmin.register Export do

  menu :priority => 1
  config.filters = false
  scope_to :current_admin_user

  permit_params :title, :content, :template, :layout_id, :tip_id, :whatsnew_id, :link

  scope :healthy_tips
  scope :whats_news

  member_action :lock, :method => :put do
    export = Export.find(params[:id])
    if export.update(:title => 'changed')
      redirect_to admin_exports_url
    end
  end

  controller do
    def create
      @export = Export.new(params.require(:export).permit(:title, :content, :link, :template, :layout_id, :tip_id, :whatsnew_id))

      respond_to do |format|
        if @export.save   

          format.html { redirect_to admin_exports_url, notice: 'Export was successfully created.' }
          format.json { render :show, status: :created, location: @export }
        else
          format.html { render :new }
          format.json { render json: @export.errors, status: :unprocessable_entity }
        end
      end
    end
  end    


  index do
    selectable_column
    column 'Export Name' do |e|
      e.title
    end

    if params[:scope] == 'whats_news'
      column "What's New Title" do |w|
        w.whatsnew.title
      end
      column "What's New" do |export|
        link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/default_third_column_home.html", :download => "default_third_column_home.shtml"
      end 

    elsif params[:scope] == 'healthy_tips'

      column 'Healthy Tip Title' do |t|
        t.tip.title
      end

      column "Content" do |export|
        link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/#{export.tip.try(:slug)}.shtml", :download => "#{export.tip.try(:slug)}.shtml"
      end
      column "Sidebar" do |export|
        link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/stay-healthy-sidebar.html", :download => "stay-healthy-sidebar.shtml"
      end
      column "Archive" do |export|
        link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/ForPatients-StayHealthy.shtml", :download => "ForPatients-StayHealthy.shtml"
      end          
    end 
    column "publish" do |export|
      link_to "publish", lock_admin_export_path(export.id), :method => :put
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :layout_id, :label => 'Layout', :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
      f.input :tip_id, :label => 'Healthy Tip', :as => :select, :collection => Tip.all.map{|l| ["#{l.title}", l.id]}
      f.input :whatsnew_id, :label => 'Whats New', :as => :select, :collection => Whatsnew.all.map{|w| ["#{w.title}", w.id]}
    end
    f.actions
  end  
end