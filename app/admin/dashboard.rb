ActiveAdmin.register_page "Dashboard" do

	menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

	content title: proc{ I18n.t("active_admin.dashboard") } do
		div class: "blank_slate_container", id: "dashboard_default_message" do
			span class: "blank_slate" do
				span I18n.t("active_admin.dashboard_welcome.welcome")
				small I18n.t("active_admin.dashboard_welcome.call_to_action")
			end
		end
	end

	page_action :ex, :method => :get do
		redirect_to admin_tips_path, :notice => "You did stuff!"
	end

	# action_item do
	# 	link_to "Do Stuff", admin_tips_path, :method => :get
	# end

	# content do
 #      panel "Tips" do
 #        table_for Tip.all do
 #          column :title
 #          column "By" do |u|
 #            u.admin_user.try(:facility_name)
 #          end
 #          column "Date" do |d|
 #            d.created_at.strftime("%d %b %y")
 #          end
 #        end
 #      end  
 #      panel "In the News" do
 #        table_for Event.all do
 #          column :title
 #          column "By" do |u|
 #            u.admin_user.try(:facility_name)
 #          end
 #          column "Date" do |d|
 #            d.created_at.strftime("%d %b %y")
 #          end
 #        end
 #      end       
 #        panel "Facility Stats" do
 #          table_for AdminUser.all do
 #            column :facility_name
 #            # column "By" do |u|
 #            #   u.admin_user.try(:facility_name)
 #            # end
 #            column "Healthy tips" do |d|
 #              d.tips.count
 #            end
 #            column "Press Releases" do |d|
 #              d.releases.count
 #            end
 #            column "In the News" do |d|
 #              d.events.count
 #            end            
 #          end
 #        end         
	# end


	# sidebar :help do
	# 	"Need help? Email us at help@example.com"
	# end


	# sidebar :help do
	# 	"Need help? Email us at help@example.com"
	# end  

end