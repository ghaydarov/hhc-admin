ActiveAdmin.register Tip do

  menu :label => "Healthy Tips"

  permit_params :title, :short, :content, :imge_url, :slug, :tip_reference, :small_image, :layout_id
  
  config.filters = false

  # action_item do
  #   link_to "Do Stuff", admin_tips_path, :method => :get
  # end

  # action_item do
  #   link_to "Do Stuff", admin_tips_path, :method => :get
  # end
  
  scope_to :current_admin_user
 
  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  index do 
    selectable_column
    column :id
    column :title
    column :imge_url  do |tip|
      image_tag(tip.imge_url, width: 175)
    end    
    column "URL", :slug
    actions
    # column "Content" do |tip|
    #   link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/#{tip.try(:slug)}.shtml", :download => "#{tip.try(:slug)}.shtml"
    # end
    # column "Sidebar" do |tip|
    #   link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/stay-healthy-sidebar.html", :download => "stay-healthy-sidebar.shtml"
    # end
    # column "Archive" do |tip|
    #   link_to "download", "https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/ForPatients-StayHealthy.shtml", :download => "ForPatients-StayHealthy.shtml"
    # end          


    # content do
    #     panel "Tips" do
    #       table_for Tip.all do
    #         column :title
    #         column "By" do |u|
    #           u.admin_user.try(:facility_name)
    #         end
    #         column "Date" do |d|
    #           d.created_at.strftime("%d %b %y")
    #         end
    #       end
    #     end                      
    # end    
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :layout_id, :as => :hidden, :value => Layout.find(1).id
      f.input :short, label: 'Short'
      f.input :content, class: "mceEditor"
      f.input :tip_reference
      f.input :imge_url, label: 'Main Image URL'
      f.input :small_image, label: 'Small Image URL'
      unless f.object.new_record?
        f.input :slug
      end
    end
      f.actions
  end

end