ActiveAdmin.register Layout do
  permit_params :layout_name, :head, :title, :og_title, :og_description, :og_image, :body_top, :body_bottom, :sidebar, :tip_reference, :position
  
  config.filters = false
  menu :label => "Layouts"
  menu :parent => "Settings"
  


  scope_to :current_admin_user

  
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported

  sortable # creates the controller action which handles the sorting



  index do
    selectable_column
    sortable_handle_column # inserts a drag handle

    column :id
    column :position
    column :layout_name
    actions
  end

  form do |f|
    f.inputs do
      f.input :layout_name
      f.input :head, :input_html => { :class => 'noEditor' }
      f.input :body_top, :input_html => { :class => 'noEditor' }
      f.input :tip_reference, :input_html => { :class => 'noEditor' }
      f.input :body_bottom, :input_html => { :class => 'noEditor' }
    end
    f.actions
  end    
end