ActiveAdmin.register Slide do


  menu label: "Heroes"
  menu :parent => "Home Page"

  permit_params :title, :description, :image_url, :thumb_url, :link, :slug, :date, :admin_user_id, :layout_id, :position
  scope_to :current_admin_user
  scope :active, default: true
  scope :archived
  config.filters = false
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported
  sortable # creates the controller action which handles the sorting


  member_action :archive, :method => :put do
  slider = Slide.find(params[:id])
    if slider.nonarchive? || slider.status == nil
      slider.archive!
      redirect_to admin_slides_url
    end
  end

  member_action :activate, :method => :put do
  slider = Slide.find(params[:id])
    if slider.archive? || slider.status == nil
      slider.nonarchive!
      redirect_to admin_slides_url
    end
  end

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end

    # def collection
    #   scoped_collection.where(status: 0).page(params[:page]).per(10).order("position asc")
    # end
  end
  
  index :title => "Hero List" do

    selectable_column # inserts a drag handle
    sortable_handle_column # inserts a drag handle
    column :position
    column :title

    column "change status" do |slide|
      if slide.archive? 
        link_to "activate", activate_admin_slide_url(slide.id), :method => :put
      else
        link_to "archive it", archive_admin_slide_url(slide.id), :method => :put
      end
    end
    column :description
    # column "Image"  do |slide|
      # dir = slide.image_url
      # dir.slice!(0..4)

      # image_tag(current_admin_user.local_directory + dir, width: 175)
    # end    
    # column 'Thumbnail'  do |slide|
    #   image_tag(slide.thumb_url, width: 65)
    # end        
    # column :link
    # column :date
    # column "URL", :slug

    actions
  end


  form do |f|
    f.inputs do
      f.input :admin_user_id, :as => :hidden, :value => AdminUser.find(current_admin_user).id
      f.input :title, :input_html => {:placeholder => "Hero Title" }
      f.input :description, :input_html => { :class => 'noEditor', :placeholder => "Hero Text" }
      f.input :image_url, :input_html => { :value => "../../images/hero/your-hero-image.jpg" }
      f.input :thumb_url, :input_html => { :value => "../../images/hero/thumbs/hero-thumb-image.jpg" }
      f.input :link, :input_html => { :value => "../../html/folder/file.shtml" }
      # f.input :position
      # f.input :date, :as => :date_picker

      unless f.object.new_record?
        f.input :slug
      end      

    end
      f.actions
  end
end
