ActiveAdmin.register Setting do


  menu :label => "Set Layout"
  menu :parent => "Settings"
  

  permit_params :name, :healthy_tip_layout, :healthy_tip_archive_layout, :in_the_news_layout, :in_the_news_archive_layout, :hero_layout
  config.filters = false


  form do |f|
    f.inputs do
      f.input :name
      f.input :healthy_tip_layout, :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
      f.input :healthy_tip_archive_layout, :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
      f.input :in_the_news_layout, :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
      f.input :in_the_news_archive_layout, :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
      f.input :hero_layout, :as => :select, :collection => Layout.all.map{|l| ["#{l.layout_name}", l.id]}
    end
      f.actions
  end


  index do 
    selectable_column
    column "Settings"  do |setting|
      link_to "#{setting.name} Settings", edit_admin_setting_url(setting)
    end    
  end

end
