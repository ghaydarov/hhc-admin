ActiveAdmin.register Event do


  permit_params :title, :short, :content, :status, :image_url, :month_year
  scope_to :current_admin_user
  menu :label => "In The News"
  config.filters = false
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported
  sortable # creates the controller action which handles the sorting

  # controller do
  #   def find_resource
  #     scoped_collection.friendly.find(params[:id])
  #   end
  # end
  
    # controller do
    #   # This code is evaluated within the controller class

    #   def update
    #      event = Event.find(params[:id])
    #      event.update(params[:event])
    #      redirect_to admin_events_url
    #   end
    # end


  member_action :feature, :method => :put do
  old_featured_event = Event.find_by(status: 0)
  event = Event.find(params[:id])
    unless old_featured_event.nil?
      old_featured_event.regular!    
    end
    event.update(position: 1, status: 0)
    redirect_to admin_events_url
  end

  # member_action :regular, :method => :put do
  # event = Event.find(params[:id])
  #   if event.featured? || event.status.nil?
  #     event.regular!
  #     redirect_to admin_events_url
  #   end
  # end

  index :title => "In The News List" do
    sortable_handle_column # inserts a drag handle
    column :position
    column :title
    column :status
    column "change status" do |event|
      unless event.featured? 
        link_to "make featured", feature_admin_event_url(event.id), :method => :put
      end
    end

    column "Date" do |event|
      event.month_year
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :status, :as => :hidden
      f.input :content, class: "coming"
      f.input :status, as: :select, :include_blank => false, collection: Event.statuses.keys.to_a      
      f.input :image_url
      f.input :month_year, :as => :date_picker

    end
    f.actions
  end
end