ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :facility_name, :role, :local_directory
  config.filters = false

  menu :label => "Users"
  menu :parent => "Settings"
  
  index do
    selectable_column
    id_column
    column :facility_name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end
  
  form do |f|
    f.inputs "Admin Details" do
      f.input :facility_name
      f.input :email
      f.input :local_directory
      f.input :role, as: :select, collection: AdminUser.roles.keys.to_a      
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end
