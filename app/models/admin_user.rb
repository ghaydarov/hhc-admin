class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable
  
  enum role: ['facility', 'admin']

  has_many :events
  has_many :exports
  has_many :layouts
  has_many :releases
  has_many :slides
  has_many :tips
  has_many :whatsnew
end