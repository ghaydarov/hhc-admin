class Slide < ActiveRecord::Base

  extend FriendlyId
  friendly_id :title, use: :slugged
  belongs_to :admin_user
  belongs_to :layout
  validates :slug, uniqueness: true

  enum status: { archive: 1, nonarchive: 0}
  

  after_save :generate_hero_content_link
  after_save :generate_hero_content
  after_save :generate_hero_content_js
  after_save :generate_hero
  # after_create :position_one
  
  after_destroy :generate_hero_content_link
  after_destroy :generate_hero_content
  after_destroy :generate_hero_content_js
  after_destroy :generate_hero
  acts_as_list add_new_at: :top

  scope :archived, -> { where(status: 1) }
  scope :active, -> { where(status: 0) }

  # def position_one
  # 	self.update(position: 1)
  # end

	def generate_hero
		File.open('default_header_home_hero' + '.html', "w") do |f|     
			nlink = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/hero_content_link.html"))
			n = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/hero_content.html"))
			js = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/hero_content_js.html"))
			layout = Layout.find_by(layout_name: "Hero")
				f.write(
					layout.head + "\n""\n""\n" +
					nlink.css('a.bx-pager-item').to_s + "\n""\n""\n" +
					layout.body_top + "\n""\n""\n" +
					n.css('li').to_s + "\n""\n""\n" +
					layout.tip_reference +
					js.css('li').text + "\n" +
					layout.body_bottom
					)
			uploader = FileUploader.new
      		uploader.store_dir = "repo/hhc-corp/includes/content/branding"
			uploader.store!(f)  
		end     			
	end


	def generate_hero_content_link
		File.open('hero_content_link' + '.html', "w") do |f|     
				
				Slide.where(status: 0).limit(4).order('position asc').each_with_index do |s, index|
				f.write('<a class="bx-pager-item blink" style="margin-right: 3px;" data-slide-index="' + "#{index}" + '" href="">' +
						 '<img src="' + "#{s.thumb_url}" + '" width="39" height="39" border="0" />' +
						 '</a>' + "\n""\n""\n")
				end

			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end	

	def generate_hero_content_js
		File.open('hero_content_js' + '.html', "w") do |f|     
				
				Slide.where(status: 0).limit(4).order('position asc').each_with_index do |s, index|
					f.write('<li>case ' + "#{index}" + ':' + ' change = "' + "#{s.link}" + '?hero=HHC' + "#{index + 1}" + '";break;</li>' + "\n")
				end

			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end		

	def generate_hero_content
		File.open('hero_content' + '.html', "w") do |f|     
				
				Slide.where(status: 0).limit(4).order('position asc').each_with_index do |s, index|
				f.write('<li> ' +
                  '<div class="sliderbox" onclick="changeURL(' + "#{index}" + ')" style="background:url(' + "#{s.image_url}" + ');">' +
                  '<div class="textbox" onclick="changeURL(\'stop\')"> <span class="txt1">' + "#{s.title}" + '</span>' +  "#{s.description}" + '<span class="link"><a onclick="changeURL(' + "#{index}" + '); return false;" href="">Learn More</a></span> </div></div></li>'
                )
				end

			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end	

end