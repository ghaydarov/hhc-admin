class Whatsnew < ActiveRecord::Base
  # has_many :exports
  belongs_to :layout
  belongs_to :admin_user

  after_save :generate_whats_new_content
  after_save :generate_whats_new

  after_destroy :generate_whats_new_content
  after_destroy :generate_whats_new
  acts_as_list add_new_at: :top

  enum status: { archive: 1, nonarchive: 0}

  scope :archived, -> { where(status: 1) }
  scope :active, -> { where(status: 0) }


	def generate_whats_new
		File.open('default_third_column_home' + '.html', "w") do |f|     
			n = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/whatsnew_content.html"))
				layout = Layout.find_by(layout_name: "What's New?")			
				f.write(
					layout.body_top + "\n""\n""\n" +
					n.css('div.contentdiv').to_s + "\n""\n""\n" +
					layout.body_bottom
					)
			uploader = FileUploader.new
      		uploader.store_dir = "repo/hhc-corp/includes/content/column_modules"
			uploader.store!(f)
		end     			
	end


	def generate_whats_new_content
		File.open('whatsnew_content' + '.html', "w") do |f|     
				
				Whatsnew.where(status: 0).limit(5).order(position: :asc).each_with_index do |w, index|
				f.write(					
					"<div class='contentdiv'>
					<div class='cropdiv'>
					<img style='width:165px;' src='" + "#{w.image}" + "' /></div><a href='#{w.link + '?whatsnew='}#{index + 1}'" + "> #{w.title} </a>" +
					"<div class='text'>" + "#{w.content}" + "</div>"  + "\n" +
					"<div class='fullstory'><a href='#{w.link + '?whatsnew='}#{index + 1}'> Full Story </a></div></div>")
				end

			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end	

end


