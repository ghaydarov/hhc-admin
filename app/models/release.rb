class Release < ActiveRecord::Base

	extend FriendlyId
	friendly_id :title, use: :slugged
	
	validates_length_of :facebook_title, maximum: 88

	belongs_to :admin_user
  	belongs_to :layout
	enum status: ['featured', 'regular']
  	after_save :generate_file  
  	after_save :generate_featured, :unless => lambda { |record| record.regular? }  
  	after_save :generate_regular
  	after_save :generate_press_releases
	# before_save :disable_old_featured, :unless => lambda { |record| record.regular? }
  	
  	after_destroy :generate_file  
  	after_destroy :generate_featured #, :unless => lambda { |record| record.regular? }  
  	after_destroy :generate_regular
  	after_destroy :generate_press_releases	
	acts_as_list add_new_at: :top
	

  	# after_destroy :generate_featured  
  	# after_destroy :generate_regular
  	# after_destroy :generate_press_releases


   # include ActionController::Base.helpers.link_to
   include ActionView::Helpers::UrlHelper


  def generate_file
  	File.open(self.slug + '.shtml', "w") do |f|     
  			layout = Layout.find_by(layout_name: 'Press Releases')

  		f.write(
  			layout.head + "\n" +
  			"<title>#{self.title}</title>" + "\n" +
  			'<meta property="og:title"' + ' content="' + "#{self.facebook_title}" + '" />' + "\n" +
  			'<meta property="og:description"' + ' content="' + "#{self.short.strip}" + '" />' + "\n" +
  			'<meta property="og:image"' + ' content="' + "#{self.image_url}" + '" />' + "\n" +
  			layout.body_top + "\n" + "#{self.date_time}" + "</span></p>" + "\n" +
  			'<p class="heading2-article-alias">' + "#{self.title}" + '</p>' + "\n" +
  			'<p class="heading2-article" style="display:none;">' + "#{self.title}" + '</p>' + "\n" +
  			self.content + "\n" +
  			'<p class="dottedseparator">&nbsp;</p>' + "\n" +
  			self.contact + "\n" +
  			layout.body_bottom 
  			)
  		uploader = FileUploader.new
      	uploader.store_dir = "repo/hhc-corp/html/news"
  		uploader.store!(f)	
  	end
  end


	def generate_press_releases
		layout = Layout.find_by(layout_name: 'Press Releases Listing')
		File.open('News-PressReleases' + '.shtml', "w") do |f|     
			fp = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/featured_press.html"))
			r = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/regular_press.html"))
			f.write(
				layout.head + "\n" "\n" +
				fp.css('div.feature-pressreleases').to_s + "\n""\n""\n" +
				r.css('div.newsitem').to_s + "\n""\n""\n" +
				layout.body_bottom
				)
			uploader = FileUploader.new
			uploader.store_dir = "repo/hhc-corp/html/news"
			uploader.store!(f)	
		end
	end  


	# def disable_old_featured
	# 	release = Release.find_by(status: 0)
	# 	unless release.nil?
	# 		release.regular!
	# 	end
	# end

	def generate_featured
		File.open('featured_press' + '.html', "w") do |f|
			press = self
			f.write('<div class="feature-pressreleases" style="padding-bottom:0px;">
				<img src="' + press.image_url + '" alt="' + press.title + '"/>' +
				'<div style="padding:10px;">' + link_to(press.title, '../../html/news/' + press.slug + ".shtml") + '<br>
				<br><span class="footer_about"><strong>' + press.date_time + '</strong></span>
                    <div style="clear:both;"></div>
                  </div>
                </div><br />')
			uploader = FileUploader.new
			uploader.store!(f)
		end
	end


	def generate_regular
		File.open('regular_press' + '.html', "w") do |f|
			Release.where(status: 1).order('position asc').each do |press|
				f.write('<div class="newsitem">' + link_to(press.title, '../../html/news/' + press.slug + ".shtml") + '<p>' + press.short + '<br>' + '<span class="footer_about"><strong>' + press.date_time + '</strong></span></p><div style="clear:both;"></div></div>')
			end
			uploader = FileUploader.new
			uploader.store!(f)
		end
	end  

end



