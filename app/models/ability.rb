class Ability
  include CanCan::Ability

    def initialize(user)
      if user.admin?  
        can :manage, Tip
        can :manage, AdminUser
        can :manage, Setting
        can :manage, Release
        can :manage, Layout
        can :manage, Event
        can :manage, Whatsnew
        can :manage, Slide
        can :manage, ActiveAdmin::Page, :name => "Dashboard"
      elsif user.facility?
        can :manage, Tip
        can :manage, Layout
        # can :manage, Whatsnew
        can :read, ActiveAdmin::Page, :name => "Dashboard"
        # can :manage, AdminUser.where(id: user.id).last
        
      else
        can :manage, Tip
        can :read, ActiveAdmin::Page, :name => "Dashboard"
      end
    end
end
