class Tip < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  
  has_many :exports
  belongs_to :admin_user
  belongs_to :layout
  
  after_save :generate_file  
  after_save :generate_arhiw_file
  after_save :generate_arhiw
  after_save :generate_sidebar
  
  after_destroy :generate_arhiw_file
  after_destroy :generate_arhiw
  after_destroy :generate_sidebar

  def generate_file
  	File.open(self.slug + '.shtml', "w") do |f|     
  		f.write(
  			self.layout.head + "\n" +
  			"<title>#{self.title}</title>" + "\n" +
  			'<meta property="og:title"' + ' content="' + "#{self.title}" + '" />' + "\n" +
  			'<meta property="og:description"' + ' content="' + "#{self.short}" + '" />' + "\n" +
  			'<meta property="og:image"' + ' content="' + "#{self.imge_url}" + '" />' + "\n" +
  			self.layout.body_top + "\n" +
  			'<p class="heading2-article" style="display:none;">' + "#{self.title}" + '</p>' + "\n" +
  			'<img title="" border=0 alt="" align="center" src="' + "#{self.imge_url}"  +'" id="pin" width="468" >' + "\n" +
  			self.content + "\n" +
  			'<p class="dottedseparator">&nbsp;</p>' + "\n" +
  			self.tip_reference + "\n" +
  			self.layout.tip_reference + "\n" +
  			self.layout.body_bottom 
  			)
  		uploader = FileUploader.new
      uploader.store_dir = "repo/hhc-corp/html/patients"
  		uploader.store!(f)	
  	end
  end

  def generate_sidebar
  	File.open('stay-healthy-sidebar' + '.html', "w") do |f|     
  		admin_user.tips.all.each do |t|
  			f.write('
  				<div style="border-bottom: 1px dotted #D6D6D6;" class="' + t.try(:slug) + '">     
  				<a href="' + t.try(:slug) + '.shtml">
  				<img src="' + t.small_image.to_s + '" class="healthy-thumb">
  				</a>
  				<h3> 
  				<a href="' + t.try(:slug) + '.shtml">' + t.title + '</a>
  				</h3>
  				</div>
  				')
  		end
  		uploader = FileUploader.new
      uploader.store_dir = "repo/hhc-corp/includes/content/column_modules"
  		uploader.store!(f)  
  	end     	
  end

  def generate_arhiw
  	layout = Layout.find(2)
  	File.open('ForPatients-StayHealthy' + '.shtml', "w") do |f|     
  		n = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/arhiw.html"))
  		f.write(
  			layout.head + "\n" +
  			layout.body_top + "\n" "\n""\n" +
  			n.css('div.tip').to_s + "\n""\n""\n" +
  			layout.body_bottom
  			)
  		uploader = FileUploader.new
      uploader.store_dir = "repo/hhc-corp/html/patients"
  		uploader.store!(f)	
  	end
  end  

  def generate_arhiw_file
  	File.open('arhiw' + '.html', "w") do |f|
  		admin_user.tips.all.each do |t|
  			f.write('
  				<div style="width: 690px; float:left;" class="tip">
  				<div style="float: left; margin-right: 15px;">  '  + "\n" +
  				'<a href="' + t.try(:slug) + '.shtml">'  + "\n" +
  				'<img src="' + t.small_image.to_s + '" class="healthy-thumb">'  + "\n" +
  				'</a>'  + "\n" +
  				'</div>'  + "\n" +
  				'<div>'  + "\n" +
  				'<h3>' + "\n" +
  				'<a href="' + t.try(:slug) + '.shtml">' + t.title + '</a>'  + "\n" +
  				'</h3>'  + "\n" +
  				'<p>' + t.short + '</p> ' + "\n" +
  				'<a href="' + t.try(:slug) + '.shtml">' + "\n" +
  				'<img src="../../images/healthy/learn-more.jpg" class="healthy-thumb">' + "\n" +
  				'</a>' + "\n" +
  				'</div>' + "\n" +
  				'<br>' + "\n" +
  				'<br> ' + "\n" +
  				'</div>')
  		end
  		uploader = FileUploader.new
  		uploader.store!(f)
  	end
  end
end
