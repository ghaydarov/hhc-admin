class Event < ActiveRecord::Base
	# extend FriendlyId
	# friendly_id :title, use: :slugged
	belongs_to :admin_user
	enum status: ['featured', 'regular']


	after_save :generate_featured
	after_save :generate_regular
	after_save :generate_in_the_news
	after_save :generate_arhiw
	after_save :generate_in_the_news_archive
	# before_save :disable_old_featured, :unless => lambda { |record| record.regular? }
	after_destroy :generate_regular
	after_destroy :generate_in_the_news
	after_destroy :generate_arhiw
	after_destroy :generate_in_the_news_archive

	# acts_as_list
	acts_as_list add_new_at: :top


	# def disable_old_featured
	# 	event = Event.where(status: 0)
	# 	event.each do |e|
	# 		e.regular!
	# 	end
	# end


	def generate_featured
		File.open('featured_in_the_news' + '.html', "w") do |f|
			event = self
			f.write('<div class="feature-inthenews">
				<img src="' + event.image_url + '" width="468" border="0" style="margin-bottom: 5px;" class="feature-itn" alt="' + event.title + '"/>' +
				'<br/><p style="margin-bottom: -10px;"><b>' + event.title + '</b></p><br/>
				<div style="">' + event.content + '</div></div>')
			uploader = FileUploader.new
			uploader.store!(f)

		end
	end

	def generate_regular
		File.open('regular_in_the_news' + '.html', "w") do |f|
			Event.all.where(status: 1).order('position asc').each do |event|
				f.write('<div class="inthenews">' + '<p style="padding-top: 10px; padding-left: 0px;"><b>' + event.title + '</b></p>' + event.content + '</div>')
			end
			uploader = FileUploader.new
			uploader.store!(f)
		end
	end  

	def generate_arhiw
		File.open('arhiw_in_the_news' + '.html', "w") do |f|
			Event.all.order('position asc').each do |event|
				f.write('<div class="inthenews-archive">' + '<p style="padding-top: 10px; padding-left: 0px;"><b>' + event.title + '</b></p>' + event.content + '</div>')
			end
			uploader = FileUploader.new
			uploader.store!(f)
		end
	end  


	def generate_in_the_news
		layout = Layout.find(4)
		File.open('News-InTheNews' + '.shtml', "w") do |f|     
			fn = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/featured_in_the_news.html"))
			r = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/regular_in_the_news.html"))
			f.write(
				layout.head + "\n" +
				layout.body_top + "\n" "\n""\n" +
				fn.css('div.feature-inthenews').to_s + "\n""\n""\n" +
				r.css('div.inthenews').to_s + "\n""\n""\n" +
				layout.body_bottom
				)
			uploader = FileUploader.new
			uploader.store_dir = "repo/hhc-corp/html/news"
			uploader.store!(f)	
		end
	end  

	def generate_in_the_news_archive
		layout = Layout.find(5)
		File.open('InTheNews-Archive' + '.shtml', "w") do |f|     
			r = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/arhiw_in_the_news.html"))
			f.write(
				layout.head + "\n" +
				r.css('div.inthenews-archive').to_s + "\n""\n""\n" +
				layout.body_bottom
				)
			uploader = FileUploader.new
			uploader.store_dir = "repo/hhc-corp/html/news"
			uploader.store!(f)	
		end
	end  	

end
