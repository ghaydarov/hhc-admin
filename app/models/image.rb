class Image < ActiveRecord::Base

  mount_uploader :hero_thumb, PhotoUploader

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :crop_thumb
  
  def crop_thumb
    hero_thumb.recreate_versions! if crop_x.present?
    # redirect_to root_url
  end

end
