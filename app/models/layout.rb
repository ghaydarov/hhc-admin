class Layout < ActiveRecord::Base
	has_many :exports
	has_many :tips
	has_many :whatsnew
	has_many :releases
	has_many :slides
	belongs_to :admin_user
	acts_as_list
end
