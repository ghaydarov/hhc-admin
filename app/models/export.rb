class Export < ActiveRecord::Base

	belongs_to :layout
	belongs_to :tip
	belongs_to :whatsnew
	belongs_to :admin_user
	mount_uploader :link, FileUploader

	after_save :generate_arhiw_file, :unless => lambda { |entry| entry.tip_id.nil? }
	after_save :generate_arhiw, :unless => lambda { |entry| entry.tip_id.nil? }
	after_save :generate_file, :unless => lambda { |entry| entry.tip_id.nil? }
	after_save :generate_sidebar, :unless => lambda { |entry| entry.tip_id.nil? }
	after_save :generate_whats_new_content, :unless => lambda { |entry| entry.whatsnew_id.nil? }
	after_save :generate_whats_new, :unless => lambda { |entry| entry.whatsnew_id.nil? }


	# 2   scope :healthy_tips, -> { where('tip_id != ?', nil) }

	def self.healthy_tips
		where(whatsnew_id: nil)
	end

	def self.whats_news
		where(tip_id: nil)
	end

	def generate_whats_new
		File.open('default_third_column_home' + '.html', "w") do |f|     
			n = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/whatsnew_content.html"))

				f.write(
					self.layout.body_top + "\n""\n""\n" +
					n.css('div.contentdiv').to_s + "\n""\n""\n" +
					self.layout.body_bottom
					)
			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end


	def generate_whats_new_content
		File.open('whatsnew_content' + '.html', "w") do |f|     
				Whatsnew.all.each do |w|
				f.write(
					'<div class="contentdiv">' + "\n" +
					'<div class="cropdiv">' + "\n" +
					'<img style="width:165px;" src="' + w.image.to_s + '" /></div>'  + "\n" +
					'<a href="' +  w.try(:link) +'">' +  w.title + '</a>'  + "\n" +
					'<div class="text">' + w.try(:content) + '</div>'  + "\n" +
					'<div class="fullstory">' + "\n" +
					'<a href="' + w.link + '">' + 'Full Story </a>' + "\n" +
					'</div></div>'
					)
				end

			uploader = FileUploader.new
			uploader.store!(f)  
		end     			
	end	

	private

	def generate_arhiw_file
		File.open('arhiw' + '.html', "w") do |f|     

			Tip.all.each do |t|
				f.write('
					<div style="width: 690px; float:left;" class="tip"> 
						<div style="float: left; margin-right: 15px;">  '  + "\n" + 
							'<a href="' + t.try(:slug) + '.shtml">'  + "\n" + 
								'<img src="' + t.small_image.to_s + '" class="healthy-thumb">'  + "\n" +
							'</a>'  + "\n" +
						'</div>'  + "\n" +
						'<div>'  + "\n" +
						'<h3>' + "\n" +
							'<a href="' + t.try(:slug) + '.shtml">' + t.title + '</a>'  + "\n" +
						'</h3>'  + "\n" +
						'<p>' + t.short + '</p> ' + "\n" +
							'<a href="' + t.try(:slug) + '.shtml">' + "\n" +
								'<img src="../../images/healthy/learn-more.jpg" class="healthy-thumb">' + "\n" +
							'</a>' + "\n" +
						'</div>' + "\n" +
				'<br>' + "\n" +
                '<br> ' + "\n" +
					'</div>')
			end
			uploader = FileUploader.new
			uploader.store!(f)  
		end     	
	end


	def generate_file
		File.open(self.tip.slug + '.shtml', "w") do |f|     
			f.write(
				self.layout.head + "\n" +
				"<title>#{self.tip.title}</title>" + "\n" +
				'<meta property="og:title"' + ' content="' + "#{self.tip.title}" + '" />' + "\n" +
				'<meta property="og:description"' + ' content="' + "#{self.tip.short}" + '" />' + "\n" +
				'<meta property="og:image"' + ' content="' + "#{self.tip.imge_url}" + '" />' + "\n" +
				self.layout.body_top + "\n" +
				'<p class="heading2-article" style="display:none;">' + "#{self.tip.title}" + '</p>' + "\n" +
				'<img title="" border=0 alt="" align="center" src="' + "#{self.tip.imge_url}"  +'" id="pin" width="468" >' + "\n" +
				self.tip.content + "\n" +
				'<p class="dottedseparator">&nbsp;</p>' + "\n" +
				self.tip.tip_reference + "\n" +
				self.layout.tip_reference + "\n" +
				self.layout.body_bottom 
				)
			uploader = FileUploader.new
			uploader.store!(f)	
		end

	end

	def generate_sidebar
		File.open('stay-healthy-sidebar' + '.html', "w") do |f|     

			Tip.all.each do |t|
				f.write('
					<div style="border-bottom: 1px dotted #D6D6D6;">     
					<a href="' + t.try(:slug) + '.shtml">
					<img src="' + t.small_image.to_s + '" class="healthy-thumb">
					</a>
					<h3> 
					<a href="' + t.try(:slug) + '.shtml">' + t.title + '</a>
					</h3>
					</div>
					')
			end
			uploader = FileUploader.new
			uploader.store!(f)  
		end     	
	end


	def generate_arhiw
		layout = Layout.find(2)
		File.open('ForPatients-StayHealthy' + '.shtml', "w") do |f|     
			n = Nokogiri::HTML(open("https://s3-us-west-2.amazonaws.com/hhcheroku/uploads/arhiw.html"))

			f.write(
				layout.head + "\n" +
				layout.body_top + "\n" "\n""\n" +
				 n.css('div.tip').to_s + "\n""\n""\n" +
				layout.body_bottom
				)
			uploader = FileUploader.new
			uploader.store!(f)	

		end

	end


end





