//= require jquery    
//= require jquery-ui 
//= require jquery_ujs
//= require activeadmin-sortable


$(document).ready(function() {

tinyMCE.init({
    // mode : "exact",
    // editor_selector : "mceEditor",
    mode : "specific_textareas",
    editor_deselector : "noEditor",
    theme : "advanced",
    theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,link,unlink,image,code,cleanup,tablecontrols",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    plugins : 'inlinepopups,table',
    setup : function(ed) {
        // Add a custom button
        ed.addButton('mybutton', {
            title : 'My button',
            image : '/assets/plugins/example/img/example.gif',
            onclick : function() {
                // Add you own code to execute something on click
                ed.focus();
                ed.selection.setContent('Hello world!');
            }
        });
    }
});

});
